package ch.unibas.cs.gravis.scalismo
import scalismo.registration.TransformationSpace.ParameterVector

import java.util.Random
import java.io._
import java.text.SimpleDateFormat
import java.util.Calendar
import breeze.linalg.DenseVector
import scalismo.common.DiscreteVectorField
import scalismo.common.RealSpace
import scalismo.common.ScalarArray
import scalismo.common.VectorField
import scalismo.geometry._
import scalismo.image.DiscreteImageDomain
import scalismo.image.DiscreteScalarImage
import scalismo.io._
import scalismo.kernels._
import scalismo.mesh.TriangleMesh
import scalismo.mesh.MeshMetrics
import scalismo.numerics._
import scalismo.registration._
import scalismo.registration.RigidTransformationSpace
import scalismo.statisticalmodel.dataset._
import scalismo.statisticalmodel.GaussianProcess
import scalismo.statisticalmodel.LowRankGaussianProcess
import scalismo.statisticalmodel.StatisticalMeshModel
import scalismo.statisticalmodel.NDimensionalNormalDistribution
import scalismo.ui.api.SimpleAPI.ScalismoUI
import scala.math
import scalismo.utils._
import vtk._
import scala.util.Failure
import scalismo.registration.Registration

object L2Regularizer extends Regularizer {
  def apply(alpha: ParameterVector) = { val t = breeze.linalg.norm(alpha, 2); t * t }

  def takeDerivative(alpha: ParameterVector) = alpha * 2f

}

object Project2 {
  
  def registerTraining(file_path : String, testing_path : String) : (TriangleMesh, Seq[DataItem[_3D]]) = {
    
    val files = new File(file_path).listFiles
    val black_list = List("26857", "26875", "26859", "26879", "26881", "26883", "26885", "26887", "26893", "26895", "26897", "26925", "26927", "26959", "26961", "26965", "26967", "26969", "26971", "26973", "26975", "26978")

    val file_names_unfiltered = files.toIndexedSeq.map{f => f.getName().dropRight(4)}.sorted  // drop stl ending
    
    println(file_names_unfiltered)
    // Only keep the files that are not on the "black list"
    var file_names = List[String]()
    file_names_unfiltered.foreach{fn =>
      var in_blacklist = false
      black_list.foreach{bl => 
        in_blacklist = in_blacklist || (fn contains bl)}
      if (!in_blacklist){
        file_names = file_names :+ fn  // apparently, one appends like this in Scala
      }
    } 
    println("file_names")
    
//    file_names = file_names.take(10)  // FIXME: not enough...
    file_names.foreach{f => println(f)}
    
    val all_shapes : IndexedSeq[TriangleMesh]= file_names.map{ fn => 
      println("readin' and 'ducin': "+fn) 
//      reduceAndSmoothMesh(MeshIO.readMesh(new File(file_path + File.separator + fn + ".stl")).get,50000f,1)
      readReduceAndSmoothMesh(new File(file_path + File.separator + fn + ".stl"), 100000f, 1)
    }.toIndexedSeq
    println("read all files")
//    ui.show(all_shapes(3),"random shape 3")
//    ui.show(all_shapes(5),"random shape 5")
//    ui.show(all_shapes(2),"random shape 2")
    // Roughly align via ICP first
    val reference_shape = all_shapes.head
    val target_shapes = icpRegisterArray(all_shapes.toArray).toList // Only returns the tail
    
//    var data_items = Seq[DataItem[_3D]]()
    
    val gdOptimizer = GradientDescentOptimizer(100, 0.05)
//    val gdOptimizer = LBFGSOptimizer(150)
    val sampler = RandomMeshSampler3D(reference_shape, 50, 42)
    val imageMetric = MeanSquaresMetric(sampler)
    
    val zeroMean = VectorField(RealSpace[_3D], (pt:Point[_3D]) => Vector(0,0,0))
    val gausMixKernel = GaussianKernel[_3D](50f) * 100f + GaussianKernel[_3D](20f) * 50f + GaussianKernel[_3D](10f) * 20f
    val matrixValuedKernel = UncorrelatedKernel(gausMixKernel)
    val gp = GaussianProcess (zeroMean,matrixValuedKernel)
    val num_lowrank = 50
    val lowrankGP = LowRankGaussianProcess.approximateGP(gp, sampler, num_lowrank)
    val gpTransSpace = GaussianProcessTransformationSpace(lowrankGP)
    println("computed lowrank gp")
    val regularizer = L2Regularizer

    val regConfig = RegistrationConfiguration(
        optimizer = gdOptimizer,
        metric = imageMetric,
        transformationSpace = gpTransSpace,
        regularizationWeight = 0.00,
        regularizer = L2Regularizer
    )
    var counter = 0
    var data_items: IndexedSeq[DataItem[_3D]] = (file_names.tail zip target_shapes).map{ case(file_name, target_shape) =>

      
      val reference_shape_img = scalismo.mesh.Mesh.meshToDistanceImage(reference_shape)
      val target_shape_img = scalismo.mesh.Mesh.meshToDistanceImage(target_shape)
      println("created distance images for "+counter)
      val registration_iterator = Registration.iterations(regConfig)(
            fixedImage = reference_shape_img,
            movingImage = target_shape_img,
            initialParameters = DenseVector.zeros[Float](num_lowrank)
          )
          
      val augmented_iterator = for ((it, itnum) <- registration_iterator.zipWithIndex) yield {
	        println(s"object value in iteration $itnum is ${it.optimizerState.value}")
        it.registrationResult
      }
      
      val result = augmented_iterator.toSeq.last
      println("registered item " + counter)
      val data_item = new DataItem[_3D](file_name, result)
//      data_items = data_items :+ data_item
//      val shape_deformed = reference_shape.transform(result)
//      ui.show(reference_shape, "reference")
//      ui.show(shape_deformed, "deformed")
//      ui.show(target_shape, "target")
      println("freeform registered shape number " + counter)
      counter = counter + 1
      data_item
    }.toIndexedSeq
    println("Foo")
    return (reference_shape, data_items)
    
//    val landmarks = file_names.map{s =>
//      val current_path = landmarks_path + File.separator + s + ".json"
//      println("Read " + current_path + " ...")
//      LandmarkIO.readLandmarksJson[_3D](new File(current_path)).get
//    }
//    val non_empty_landmarks = landmarks.zip(file_names).filter(tpl => tpl._1.length > 0)
////    val rigidLandmarkTransformation = LandmarkRegistration.rigid3DLandmarkRegistration(landmarks(0).map(l => l.point).zip(landmarks(1).map(l => l.point)).toIndexedSeq)
//    
//    val reference_landmarks = non_empty_landmarks.head
//    val transforms = non_empty_landmarks.tail.take(10).map { floating_landmarks =>  // FIXME: Remove take()
//      println("Calculate transform for " + floating_landmarks._2 + " ...")
//      (LandmarkRegistration.rigid3DLandmarkRegistration(reference_landmarks._1.map(l => l.point).zip(floating_landmarks._1.map(l => l.point)).toIndexedSeq), floating_landmarks._2)
//    }
//    ui.show(MeshIO.readMesh(new File(file_path + File.separator + reference_landmarks._2 + ".stl")).get, "reference")
//    val transformed_shapes = transforms.map{ transform =>
//      val current_path = file_path + File.separator + transform._2 + ".stl"
//      println("Load " + current_path + " ...")
//      val current_shape = MeshIO.readMesh(new File(current_path)).get
//      println("Transform " + current_path + " ...")
//      val transformed = current_shape.transform(transform._1.inverse)
//      ui.show(transformed, transform._2)  // FIXME: Remove
//      transformed
//    }
  }
  def getDataset(filepath: String, num: Int = 0) : (Array[TriangleMesh],Array[File])  = {
    var files = new File(filepath).listFiles
    var num2 : Int = num
    if (num2 < 1) {
      num2 = files.length
    }
    files = files.take(num2)
    val dataset : Array[TriangleMesh] = files.toIndexedSeq.map{ case i =>
      val m = MeshIO.readMesh(i).get
      m
    }.toArray
    return (dataset, files)
  }
  
  def getReducedDataset(filepath: String, num: Int = -1, numTargetPoints: Float = 50000f, smooth: Float = 0f) : Array[TriangleMesh] = {
    getDataset(filepath, num)._1.map(reduceAndSmoothMesh(_, numTargetPoints, smooth)).toArray
  }
  def getReducedDatasetWithFiles(filepath: String, num: Int = -1, numTargetPoints: Float = 50000f, smooth: Float = 0f) : (Array[TriangleMesh], Array[File]) = {
    val data = getDataset(filepath, num)
    (data._1.map(reduceAndSmoothMesh(_, numTargetPoints, smooth)).toArray,data._2)
  }
  def reduceAndSmoothMesh(m: TriangleMesh, numTargetPoints: Float = 50000f, smooth: Float = 0f) : TriangleMesh = {
      var mesh = m
        
      //TODO: maybe smoothen surface here already.
      if (smooth > 0) {
        val smoother = new vtkWindowedSincPolyDataFilter()
        val pd = scalismo.utils.MeshConversion.meshToVtkPolyData(mesh)
        smoother.SetInputData(pd)
        smoother.SetNumberOfIterations(20)
        smoother.SetPassBand(0.2)
        smoother.Update()
        mesh = MeshConversion.vtkPolyDataToCorrectedTriangleMesh(smoother.GetOutput()).get
      } 
      if (mesh.numberOfPoints/numTargetPoints>1) {
        val decimate1 = new vtkDecimatePro()
        val pd = scalismo.utils.MeshConversion.meshToVtkPolyData(mesh)
        decimate1.SetInputData(pd)
        println("need to make stuff smaller")
        decimate1.SetTargetReduction(numTargetPoints/mesh.numberOfPoints) // reduces number of points by half
        decimate1.Update()
        mesh =  MeshConversion.vtkPolyDataToCorrectedTriangleMesh(decimate1.GetOutput()).get
        println("missed target by factor " + mesh.numberOfPoints/numTargetPoints + "x")
      }
      mesh
  }
  def readReduceAndSmoothMesh(file: File, numTargetPoints: Float = 50000f, smooth: Float = 0f) : TriangleMesh = {
    val stlReader = new vtkSTLReader()
    stlReader.SetFileName(file.getAbsolutePath)
    stlReader.Update()
    val errCode = stlReader.GetErrorCode()
    if (errCode != 0) {
      println("big fail in reading " + file.getName())
    }
    var meshPd = stlReader.GetOutput()
    println("read " + file.getName()) 
      if (smooth > 0) {
        val smoother = new vtkWindowedSincPolyDataFilter()
        smoother.SetInputData(meshPd)
        smoother.SetNumberOfIterations(20)
        smoother.SetPassBand(0.2)
        smoother.Update()
        meshPd = smoother.GetOutput()
        println("smoothed " + file.getName())
      } 
      if (meshPd.GetNumberOfPoints()/numTargetPoints>1) {
        val decimate1 = new vtkDecimatePro()
        decimate1.SetInputData(meshPd)
        println("need to make stuff smaller")
        decimate1.SetTargetReduction(numTargetPoints/meshPd.GetNumberOfPoints()) // reduces number of points by half
        decimate1.Update()
        meshPd = decimate1.GetOutput()
        println("reduced " + file.getName() + " (missed target by factor " + meshPd.GetNumberOfPoints()/numTargetPoints + "x)")
      }
      MeshConversion.vtkPolyDataToCorrectedTriangleMesh(meshPd).get
  }
  
  
  def icpRegisterArray(dataset: Array[TriangleMesh]): Array[TriangleMesh] = {
    println("starting method 3 for rigid registration")
    val referencepd : vtkDataSet = scalismo.utils.MeshConversion.meshToVtkPolyData(dataset(0))
    var it = 0
    val datasetregistered = dataset.toIndexedSeq.tail.map{ case m =>
      val meshAndTransform = icpRegisterMesh(referencepd, m)
      println("registered "+it+"/"+(dataset.length-1)+" mesh the icp way")
      it+= 1
      meshAndTransform._1
    }.toArray
    return datasetregistered
  }
  def avgDistance(m1: TriangleMesh, m2: TriangleMesh, samplePoints: Int = 1000): Double = {
    val rand = new Random(System.currentTimeMillis())
    val points = rand.ints(samplePoints,0,m1.numberOfPoints-1).toArray().map(m1.points(_))
//    print("got the points")
    val dists = for (ptM1 <- points) yield {
      val (cpM2, _) = m2.findClosestPoint(ptM1)
      (ptM1 - cpM2).norm
    }
    dists.sum / samplePoints
  }
  def icpRegisterMesh(referencepd: vtkDataSet, floating: TriangleMesh) : (TriangleMesh, vtkIterativeClosestPointTransform) = {
    val floatingpd : vtkDataSet = scalismo.utils.MeshConversion.meshToVtkPolyData(floating)
    
    val closestPointT: vtkIterativeClosestPointTransform = new vtkIterativeClosestPointTransform()
    closestPointT.SetSource(floatingpd)
    closestPointT.SetTarget(referencepd)
    closestPointT.GetLandmarkTransform().SetModeToRigidBody()
    closestPointT.SetMaximumNumberOfIterations(300)
    closestPointT.Modified()
    closestPointT.Update()
    
  //      val x = vtkMatrix4x4()
    
    val filter = new vtkTransformPolyDataFilter()
    filter.SetInputData(floatingpd)
    filter.SetTransform(closestPointT)
    filter.Update()
    
    (MeshConversion.vtkPolyDataToCorrectedTriangleMesh(filter.GetOutput()).get, closestPointT)
  }
  
  def main(args: Array[String]) {
    val newRegistration = false
//    val method = 2
//    
    
    scalismo.initialize()
//    
    var model : StatisticalMeshModel = null
    
    val file_path : String = args(0) //val file_path = "/home/simon/Downloads/milestone2/training" 
    val testing_path : String = args(1) //val testing_path = "/home/simon/Downloads/milestone2/toComplete"
//  
    if (newRegistration) {
      // Build data collection from training data (-> dc), then use it to create a PCA model:
      val m2rv = registerTraining(file_path, testing_path)
      val reference = m2rv._1
      val regdataset = m2rv._2
      val dc = new DataCollection(reference, regdataset)
      val pcaModel = PCAModel.buildModelFromDataCollection(dc).get
      val gpPDM = pcaModel.gp.interpolateNystrom(2*pcaModel.gp.rank)
      val PDMKernel = gpPDM.cov
      println("built pca model")
      //val zeroMean = VectorField(reference.boundingBox, (pt:Point[_3D]) => Vector(0,0,0))
      // ^ Caution: vectors from the reference shape to the current shape
      
      val vecFieldMean : DiscreteVectorField[_3D, _3D] = DiscreteVectorField(reference, (0 until reference.numberOfPoints).map{ case(int) => Vector(0.0f,0.0f,0.0f)})
      // Define a nearest neigbor interpolator for vecFieldMean
      def interpolMean(p : Point[_3D]) : Vector[_3D] = vecFieldMean(reference.findClosestPoint(p)._2)
      // Create the continuous version of vecFieldMean
      val sampleMeanDef : VectorField[_3D, _3D]=  VectorField(reference.boundingBox, interpolMean)
      
      
      // Create a "noise kernel"
      val noise_level = 1e-1
      //val noise_level = 0.0
      case class NoiseKernel(level: Double) extends PDKernel[_3D] {
        override def domain = RealSpace[_3D]
        override def k(x: Point[_3D], y: Point[_3D]) = (if (x == y) level * level else 0.0)
      }
      
      case class MaternKernelv3_2(rho: Double) extends PDKernel[_3D] {
        override def domain = RealSpace[_3D]
        override def k(x: Point[_3D], y: Point[_3D]) = (1+math.sqrt(3*(x-y).norm2)/rho)*math.exp(-math.sqrt(3*(x-y).norm2)/rho)
      }
      
      case class MaternKernelv5_2(rho: Double) extends PDKernel[_3D] {
        override def domain = RealSpace[_3D]
        override def k(x: Point[_3D], y: Point[_3D]) = (1+math.sqrt(5*(x-y).norm2)/rho+5*(x-y).norm2/(3*rho*rho))*math.exp(-math.sqrt(5*(x-y).norm2)/rho)
      }
      val matrixValuedNoiseKernel = UncorrelatedKernel[_3D](NoiseKernel(noise_level))
      val matrixValuedMaternv3_2rho2Kernel = UncorrelatedKernel[_3D](MaternKernelv3_2(2))
      val matrixValuedMaternv5_2rho2Kernel = UncorrelatedKernel[_3D](MaternKernelv5_2(2))
      
      val gp = GaussianProcess(sampleMeanDef, (PDMKernel + matrixValuedNoiseKernel)+matrixValuedMaternv3_2rho2Kernel)
      val sampler = RandomMeshSampler3D(reference, 50,42)
      val lowRankGP = LowRankGaussianProcess.approximateGP(gp, sampler, 50, 42)
      model = StatisticalMeshModel(reference, lowRankGP)
      
      StatismoIO.writeStatismoMeshModel(model,new File("ourOwnPersonalStatisticalMeshModelOfMilestone2.h5"))
    } else {
      model = StatismoIO.readStatismoMeshModel(new File("ourOwnPersonalStatisticalMeshModelOfMilestone2.h5")).get
      // FIXME:
//      val ui = ScalismoUI()
//      ui.show(model, "Liver model")
    }
//    val ui = ScalismoUI()
//    val testdata : (Array[TriangleMesh], Array[File]) = getReducedDatasetWithFiles(testing_path, -1, 100000f, 1) //FIXME: trying this by reducing the dataset too. avgdistance uses way too much computation time. 
    val testdata : (Array[TriangleMesh], Array[File]) = getDataset(testing_path)
    val referencepd = scalismo.utils.MeshConversion.meshToVtkPolyData(model.mean)
    val standardDateFormat = new SimpleDateFormat()
    val folder = Calendar.getInstance().getTimeInMillis().toString
    val thedir : File = new File(folder)
    thedir.mkdir()
    val rand = new Random(System.currentTimeMillis());
    println("thedir "+thedir.getName()+" created")
    var reconstructedMesh : TriangleMesh = null// container for reconstructed mesh
    val ui = ScalismoUI()

    val reconstructedSet = testdata._1.zip(testdata._2).map{ case (m,file)  =>
      val reconstruct_with_icp = true
      
      //register mesh to model
      //VARIANTE 1: transform via icp (used in final submission)
      if (reconstruct_with_icp) {
        val meshAndTransform = icpRegisterMesh(referencepd,m)
        ui.show(m,"not registered with hole")
        val regm = meshAndTransform._1
        ui.show(regm,"registered with hole")
        val transformation = meshAndTransform._2
        println("registered testmesh")
              //reconstruct mesh
//      val littleNoise = NDimensionalNormalDistribution(Vector(0,0,0), SquareMatrix((1f,0,0), (0,1f,0), (0,0,1f)))
        val selectedPoints = rand.ints(1000,0,regm.numberOfPoints).toArray()
  //      (0 until 100).foreach{i=>println(selectedPoints(i))}
        println("Find closest points for random landmarks ...")
        val zippedLandmarkPoints = selectedPoints.map(i => regm.points(i)).toSeq.map{
          (pp:Point[_3D]) => val temp = model.mean.findClosestPoint(pp)
          (temp._2,pp)
        }.toIndexedSeq
        val completionPosterior = model.posterior(zippedLandmarkPoints, 1f)
        println("createdPosterior")
        ui.show(completionPosterior,"die post")
  
  
  
  //      ui.show(completionPosterior,"die post")
  //      ui.show(m,"not registered with hole")
  //      ui.show(regm,"registered with hole")
        var bestFitMesh = completionPosterior.mean
        println(bestFitMesh.numberOfPoints)
        println(regm.numberOfPoints)
        println("initialized bestfitmesh")
        var bestDist = avgDistance(regm,bestFitMesh) 
        println("initialized bestdist")
        (0 until 50).foreach{i=>
           val testMesh = completionPosterior.sample
           val testDist = avgDistance(regm,testMesh)
           if (testDist < bestDist) {
             bestFitMesh = testMesh
             bestDist = testDist
           }
           println("did iteration " + i + " of " + file.getName())
           println("best distance so far: " + bestDist)
        }
  
        ui.show(bestFitMesh,"bestfit")
        println("computed best sample with " + bestDist)
  
        //transform back
        transformation.Inverse()
        val filter = new vtkTransformPolyDataFilter()
        filter.SetInputData(scalismo.utils.MeshConversion.meshToVtkPolyData(bestFitMesh))
        filter.SetTransform(transformation)
        filter.Update()
        reconstructedMesh = MeshConversion.vtkPolyDataToCorrectedTriangleMesh(filter.GetOutput()).get

      } else {
      //VARIANTE 2: transform via gaussian process in rigid space (not used in final submission)
        val gdOptimizer2 = GradientDescentOptimizer(100, 0.05)
        val sampler = RandomMeshSampler3D(model.mean, 50, 42)
        val imageMetric = MeanSquaresMetric(sampler)
        
        val zeroMean = VectorField(RealSpace[_3D], (pt:Point[_3D]) => Vector(0,0,0))
        val regularizer = L2Regularizer
    
        val rts = RigidTransformationSpace[_3D]()
        val regConfig2 = new RegistrationConfiguration(
            optimizer = gdOptimizer2,
            metric = imageMetric, 
            transformationSpace = rts,
            regularizationWeight = 0.00,
            regularizer = L2Regularizer
        )
//        println(regConfig2)
        val reference_shape_img = scalismo.mesh.Mesh.meshToDistanceImage(model.mean)
        val target_shape_img = scalismo.mesh.Mesh.meshToDistanceImage(m)
        val registration_iterator = Registration.iterations(regConfig2)(
              fixedImage = reference_shape_img,
              movingImage = target_shape_img
            )
            
        val augmented_iterator = for ((it, itnum) <- registration_iterator.zipWithIndex) yield {
          println(s"object value in iteration $itnum is ${it.optimizerState.value}")
          it.registrationResult
        }
        
        val transformation : ProductTransformation[_3D] = augmented_iterator.toSeq.last
        ui.show(TriangleMesh(m.points.map(transformation(_)).toIndexedSeq,m.cells),"did it work?")
        //Why is this rigidtransformationspace producing producttransformations? Am I finally going mad here?-.-
        val selectedPoints = rand.ints(200,0,m.numberOfPoints).toArray()
  //      (0 until 100).foreach{i=>println(selectedPoints(i))}
        val zippedLandmarkPoints = selectedPoints.map(i => m.points(i)).toSeq.map{
          (pp:Point[_3D]) => val temp = model.mean.findClosestPoint(transformation(pp))
          (temp._2,pp)
        }.toIndexedSeq
        
        val rigidTransformation = LandmarkRegistration.rigid3DLandmarkRegistration(zippedLandmarkPoints.map{case (a,b)=>(model.mean.points(a),b)})  // your landmarks here
        val alignedModel : StatisticalMeshModel = model.transform(rigidTransformation)
        
        
        val completionPosterior = alignedModel.posterior(zippedLandmarkPoints, 1f)
        println("createdPosterior")
        ui.show(completionPosterior,"die post")
  
  
  
  //      ui.show(completionPosterior,"die post")
  //      ui.show(m,"not registered with hole")
  //      ui.show(regm,"registered with hole")
        var bestFitMesh = completionPosterior.mean
        println(bestFitMesh.numberOfPoints)
        val regm : TriangleMesh = m
        println(regm.numberOfPoints)
        println("initialized bestfitmesh")
        var bestDist = avgDistance(regm,bestFitMesh) 
        println("initialized bestdist")
        (0 until 50).foreach{i=>
           val testMesh = completionPosterior.sample
           val testDist = avgDistance(regm,testMesh)
           if (testDist < bestDist) {
             bestFitMesh = testMesh
             bestDist = testDist
           }
           println("did iteration " + i + " of " + file.getName())
        }      

        reconstructedMesh = bestFitMesh

        //END VARIANTE 2

      }


      println("reconstructed mesh")
      //safe transformed mesh in folder
     
      MeshIO.writeMesh(reconstructedMesh, new File(folder + File.separator + file.getName()+"-fixed.stl"))
      println("wrote mesh to disk as "+folder + File.separator + file.getName()+"-fixed.stl")
    }
    
    
  }
  
}
