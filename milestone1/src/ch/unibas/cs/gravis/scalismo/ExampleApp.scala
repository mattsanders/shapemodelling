package ch.unibas.cs.gravis.scalismo

import java.io._

import scalismo.common.DiscreteVectorField
import scalismo.common.RealSpace
import scalismo.common.ScalarArray
import scalismo.common.VectorField
import scalismo.geometry._
import scalismo.image.DiscreteImageDomain
import scalismo.image.DiscreteScalarImage
import scalismo.io._
import scalismo.kernels._
import scalismo.mesh.TriangleMesh
import scalismo.numerics.RandomMeshSampler3D
import scalismo.registration.LandmarkRegistration
import scalismo.statisticalmodel.dataset._
import scalismo.statisticalmodel.GaussianProcess
import scalismo.statisticalmodel.LowRankGaussianProcess
import scalismo.statisticalmodel.StatisticalMeshModel
import scalismo.ui.api.SimpleAPI.ScalismoUI
import scala.math

object ExampleApp {

  def main(args: Array[String]) {
    
    scalismo.initialize()
    val ui = ScalismoUI()
    
    // Folder path to the stl files given for milestone 1 goes here:
    val file_path : String = args(0)
    // We misuse "lowresModel.h5" from the tutorial to generate unseen testing data 
    // for the generalization metric. File path goes here:
    val testing_path : String = args(1)
    
    val files = new File(file_path).listFiles
    val dataset = files.toIndexedSeq.map(MeshIO.readMesh(_).get).toArray
    
    val faceModel = StatismoIO.readStatismoMeshModel(new File(testing_path)).get

    
    // Register all members of the training dataset on first element:
    (1 until dataset.length).map {
      case(index)=> val trans = LandmarkRegistration.rigid3DLandmarkRegistration(dataset(index).points.toIndexedSeq.zip(dataset(0).points.toIndexedSeq));
      dataset(index) = dataset(index).transform(trans)
    }
    
    // Make the mean face of the training data our reference    
    val averagePoints = (0 until dataset(0).numberOfPoints).map { 
      ptId : Int => Point(dataset.toSeq.map(_(ptId).toVector(0)).sum/dataset.length, dataset.toSeq.map(_(ptId).toVector(1)).sum/dataset.length, dataset.toSeq.map(_(ptId).toVector(2)).sum/dataset.length)
    }
    val averageMesh = TriangleMesh(averagePoints, dataset(0).cells)
    val reference = averageMesh
    ui.show(reference, "reference")
        
    // Build data collection from training data (-> dc), then use it to create a PCA model:
    val dc = DataCollection.fromMeshSequence(reference, dataset)._1.get
    val pcaModel = PCAModel.buildModelFromDataCollection(dc).get
    val gpPDM = pcaModel.gp.interpolateNystrom(2*pcaModel.gp.rank)
    val PDMKernel = gpPDM.cov
        
    //val zeroMean = VectorField(reference.boundingBox, (pt:Point[_3D]) => Vector(0,0,0))
    // ^ Caution: vectors from the reference shape to the current shape
    
    val vecFieldMean : DiscreteVectorField[_3D, _3D] = DiscreteVectorField(reference, (0 until reference.numberOfPoints).map{ case(int) => Vector(0.0f,0.0f,0.0f)})
    // Define a nearest neigbor interpolator for vecFieldMean
    def interpolMean(p : Point[_3D]) : Vector[_3D] = vecFieldMean(reference.findClosestPoint(p)._2)
    // Create the continuous version of vecFieldMean
    val sampleMeanDef : VectorField[_3D, _3D]=  VectorField(reference.boundingBox, interpolMean)
    
    
    // Create a "noise kernel"
    val noise_level = 1e-1
    //val noise_level = 0.0
    case class NoiseKernel(level: Double) extends PDKernel[_3D] {
      override def domain = RealSpace[_3D]
      override def k(x: Point[_3D], y: Point[_3D]) = (if (x == y) level * level else 0.0)
    }
    
    case class MaternKernelv3_2(rho: Double) extends PDKernel[_3D] {
      override def domain = RealSpace[_3D]
      override def k(x: Point[_3D], y: Point[_3D]) = (1+math.sqrt(3*(x-y).norm2)/rho)*math.exp(-math.sqrt(3*(x-y).norm2)/rho)
    }
    
    case class MaternKernelv5_2(rho: Double) extends PDKernel[_3D] {
      override def domain = RealSpace[_3D]
      override def k(x: Point[_3D], y: Point[_3D]) = (1+math.sqrt(5*(x-y).norm2)/rho+5*(x-y).norm2/(3*rho*rho))*math.exp(-math.sqrt(5*(x-y).norm2)/rho)
    }
    val matrixValuedNoiseKernel = UncorrelatedKernel[_3D](NoiseKernel(noise_level))
    val matrixValuedMaternv3_2rho2Kernel = UncorrelatedKernel[_3D](MaternKernelv3_2(2))
    val matrixValuedMaternv5_2rho2Kernel = UncorrelatedKernel[_3D](MaternKernelv5_2(2))
    
    
    val gp = GaussianProcess(sampleMeanDef, (PDMKernel + matrixValuedNoiseKernel)+matrixValuedMaternv3_2rho2Kernel)
    val gp2 = GaussianProcess(sampleMeanDef, (PDMKernel + matrixValuedNoiseKernel)+matrixValuedMaternv5_2rho2Kernel)
    //val gp = GaussianProcess(sampleMeanDef, (PDMKernel))
//    // Create a Gaussian kernel: scaling s relatively small, scale l quite "smooth"
//    val s = 0.1
//    val l = 30.0
//    val gaussKer : PDKernel[_3D] = GaussianKernel[_3D](l) * s
//    val matrixValuedGaussian : MatrixValuedPDKernel[_3D, _3D] = UncorrelatedKernel[_3D](gaussKer)
//    val gp1 = GaussianProcess(sampleMeanDef, matrixValuedGaussian)
//    val augmentedModel = PCAModel.augmentModel(pcaModel, gp1, 50)
//    val augmentedKernel = augmentedModel.gp.interpolateNystrom(2*augmentedModel.gp.rank).cov
//    val gp = GaussianProcess(sampleMeanDef, augmentedKernel)
          
    val sampler = RandomMeshSampler3D(reference, 500,42)
    val lowRankGP = LowRankGaussianProcess.approximateGP(gp, sampler, 500, 42)
    val statModel : StatisticalMeshModel = StatisticalMeshModel(reference, lowRankGP)
    
//    for(i <- 0 to 20) {
//      val alignedSpec = ModelMetrics.specificity(statModel, dataset, 5)  // smaller value => better
//      println("alignedSpec: "+ alignedSpec)
//    }
//    for(i <- 0 to 20) {
//      // Sample some test data from "lowresModel.h5" and Register test meshes onto first element in training dataset:
//
//      val testDataset = (0 until 10).map{ptId : Int =>val testItem = faceModel.sample;       
//        val trans = LandmarkRegistration.rigid3DLandmarkRegistration(testItem.points.toIndexedSeq.zip(dataset(0).points.toIndexedSeq));
//        testItem.transform(trans)
//        }.toArray
//       // Build data collection from test data -> testDc:
//      val testDc = DataCollection.fromMeshSequence(reference, testDataset)._1.get
//      //Compute generalization metric
//      val alignedGen = ModelMetrics.generalization(statModel, testDc).get
//      println("alignedGen: " + alignedGen)
//    }
//    println("done 1")
    
    StatismoIO.writeStatismoMeshModel(statModel,new File("ourOwnPersonalStatisticalMeshModel.h5"))

//    val lowRankGP2 = LowRankGaussianProcess.approximateGP(gp2, sampler, 500, 42)
//    val statModel2 : StatisticalMeshModel = StatisticalMeshModel(reference, lowRankGP2)
//    for(i <- 0 to 20) {
//      val alignedSpec = ModelMetrics.specificity(statModel2, dataset, 5)  // smaller value => better
//      println("alignedSpec: "+ alignedSpec)
//    }
//    for(i <- 0 to 20) {
//      // Sample some test data from "lowresModel.h5" and Register test meshes onto first element in training dataset:
//
//      val testDataset = (0 until 10).map{ptId : Int =>val testItem = faceModel.sample;       
//        val trans = LandmarkRegistration.rigid3DLandmarkRegistration(testItem.points.toIndexedSeq.zip(dataset(0).points.toIndexedSeq));
//        testItem.transform(trans)
//        }.toArray
//       // Build data collection from test data -> testDc:
//      val testDc = DataCollection.fromMeshSequence(reference, testDataset)._1.get
//      //Compute generalization metric
//      val alignedGen = ModelMetrics.generalization(statModel2, testDc).get
//      println("alignedGen: " + alignedGen)
//    }
//    println("done 2")

//    val nonalignedSpec = ModelMetrics.specificity(statModel, testMesh, 5)
    
//    val dc : DataCollection = DataCollection.fromMeshSequence(dataset.head, dataset.tail)._1.get
//    val reference : TriangleMesh = dc.reference
//    reference == dataset.head
//    
    
    
    //
//    val values = imageDomain.points.zipWithIndex.map {
//      case (p, i) =>
//        if (imageDomain.index(i)(0) % 10 < 3 && imageDomain.index(i)(1) % 10 < 3) 200 else 0
//    }
//    val image = DiscreteScalarImage(imageDomain, values.toArray)


//    ui.show(image, "grid")

  }
}